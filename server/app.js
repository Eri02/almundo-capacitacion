/*
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/../client/index.html'));
});

app.get('/user', function(req, res) {
	console.log(req.query);
    res.json({name: req.query.name});
});
*/

/*Con array functions*/

const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require("body-parser");
const serveStatic = require('serve-static');
// parse body params and attach them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(serveStatic('public', {'index': ['default.html', 'default.htm']}))

app.set('view engine', 'pug');

app.get('/', (req, res)=> {
    res.render(path.join(__dirname + '/../index'));
});

app.get("/users", (req, res)=> {
  res.render(path.join(__dirname + '/../users'));
});

/*
app.get('/', (req, res) =>{
    res.sendFile(path.join(__dirname + '/../client/index.html'));
});

app.get('/user', (req, res) => {
	console.log(req.query);
    res.json({name: req.query.name});
});*/



app.get('/usuarios', (req,res)=>{
	res.sendFile(path.join(__dirname + '/../client/formulario.html'));
});

app.post('/usuarios/create',(req,res) =>{
	console.log(req.body);
	res.json(req.body);
});





app.listen(3000);
console.log('Almundo app and listening on port 3000');
